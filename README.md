# Boss Parser
* * *

Omni dungeon guide with on the fly configuration.

## Usage
* * *

| Function name                              | Description                                       |
| ------------------------------------------ | ------------------------------------------------- |
| `bp`                                       | enables/disables mod                              |
| `bp current`                               | shows current mapId and bossId (if boss is alive) |
| `bp refresh`                               | reloads data.json from file system                |
| `bp map [id] [description]`                | adds map. Saves combination of id : description   |
| `bp boss [id] [description]`               | adds boss. Saves combination of id : description  |
| `bp action [id] [description] ([message])` | add action. Saves combination of id : description |
| `bp notice (party)`                        | enables/disables (party) notices                  |

## Data format
* * *

Data uses JSON format.

```
#!json
{
    "[mapId]": { 
        "desc": "map description",
        "[bossId]": {
            "desc": "boss description",
            "[actionId]": {
                "desc": "action description",
                "msg": "message to send as notice"
            }
        }
    }
}
```

## Notes
* * *

- If description contains any whitespace character(s) wrap it in '' (single quotes)

- While using commands `bp boss` and `bp action` you must be in instance or in close proximity of boss

## Installation

1. Create directory `[toolbox_install_directory]\mods\boss-parser`
2. Download and place `module.json` inside it
3. Start `Tera Toolbox`

## TODO

- Extend actionId JSON object to include "item": { objectId, x, y, z } for spawning items (self only, client side only)
- Extend actionId JSON object to include "circle": { itemId, radius } for spawning items to mark in/out transition mechanics (self only, client side only)
- Extend `bp boss` and `bp action` with mapId and bossId parameters (enables addition on later time even outside dungeons)
