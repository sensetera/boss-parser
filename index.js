String.prototype.clr = function(hexColor) {
    return `<font color='#${hexColor}'>${this}</font>`
};

const fs = require('fs');
const path = require('path');

module.exports = function bossParser(toolbox) {
    let hooks = [];
    let enabled = false;
    let data = readJsonData();
    let config = {
        notice: {
            enabled: true,
            party: false
        }
    }
    let mapId;
    let bossId;

    toolbox.command.add('bp', commandProcessor);

    toolbox.game.me.on('change_zone', (zone) => {
        mapId = zone;
        bossId = undefined;

        if (enabled)
        {
            toolbox.command.message(mapTemplate(mapId));
        }
    });

    function hook() {
        hooks.push(toolbox.hook(...arguments));
    }

    function load() {
        hook("S_BOSS_GAGE_INFO", 3, bossGageInfoProcessor);
        hook("S_ACTION_STAGE", 9, actionStageProcessor);
        hook("S_DUNGEON_EVENT_MESSAGE", 2, dungeonEventMessageProcessor);

        enabled = true;
    }

    function unload() {
        if (hooks.length) {
            for (let h of hooks) {
                toolbox.unhook(h);
            }
            hooks = [];
        }

        enabled = false;
    }

    function commandProcessor(option, ...parametes) {
        if (option === undefined) {
            enabled = !enabled;

            if (enabled && !hooks.length) {
                load();
            } else {
                unload();
            }

            toolbox.command.message(`${(enabled ? "ON" : "OFF").clr("00FFFF")}`)
        } else if (option === 'current') {
            toolbox.command.message(mapTemplate(mapId));
            toolbox.command.message(bossTemplate(bossId));
        } else if (option === 'refresh') {
            readJsonData();

            toolbox.command.message(`Reloaded data [file: data.json] from file system`);
        } else if (option === 'notice') {
            if (parametes.length === 0) {
                config.notice.enabled = !config.notice.enabled;

                toolbox.command.message(`Notice : ${(config.notice.enabled ? "ON" : "OFF").clr("00FFFF")}`);
            } else if (parametes[0] === 'party') {
                config.notice.party = !config.notice.party;

                toolbox.command.message(`Notice (Party) : ${(config.notice.party ? "ON" : "OFF").clr("00FFFF")}`);
            }
        } else if (option === 'map') {
            if (parametes.length < 2) {
                toolbox.command.message("map requires 2 parameters: [id] [description]");

                return;
            }

            if (!data[parametes[0]]) {
                data[parametes[0]] = {
                    desc: null
                };
            }
            data[parametes[0]].desc = parametes[1];
            saveJsonData();

            toolbox.command.message(`Added ${"Map".clr("FDD017")}(${parametes[0].clr("00FFFF")} : ${parametes[1].clr("00FFFF")})`);
        } else if (option === 'boss') {
            if (parametes.length < 2) {
                toolbox.command.message("boss requires 2 parameters: [id] [description]");
                return;
            }

            if (!data[mapId]) {
                toolbox.command.message("Unknown map. Use 'bp map [id] [description]' command to add it.");
                toolbox.command.message("Tip: Use 'bp current' command to find out [id]");
                return;
            }

            if (!data[mapId][parametes[0]]) {
                data[mapId][parametes[0]] = {
                    desc: null
                };
            }
            data[mapId][parametes[0]].desc = parametes[1];
            saveJsonData();

            toolbox.command.message(`Added ${"Boss".clr("FDD017")}(${parametes[0].clr("00FFFF")} : ${parametes[1].clr("00FFFF")})`);
        } else if (option === 'action') {
            if (parametes.length < 2) {
                toolbox.command.message("action requires 2 (3) parameters: [id] [description] ([message])");
                return;
            }

            if (!data[mapId]) {
                toolbox.command.message("Unknown map. Use 'bp map [id] [description]' command to add it.");
                toolbox.command.message("Tip: Use 'bp current' command to find out [id]");
                return;
            }

            if (!data[mapId][bossId]) {
                toolbox.command.message("Unknown boss. Use 'bp boss [id] [description]' command to add it.");
                toolbox.command.message("Tip: Use 'bp current' command to find out [id]");
                return;
            }

            if (!data[mapId][bossId][parametes[0]]) {
                data[mapId][bossId][parametes[0]] = {
                    desc: null,
                    msg: null
                };
            }
            data[mapId][bossId][parametes[0]].desc = parametes[1];

            if (parametes.length > 2)
            {
                data[mapId][bossId][parametes[0]].msg = parametes[2];
            }

            saveJsonData();

            toolbox.command.message(`Added ${"Action".clr("FDD017")}(${parametes[0].clr("00FFFF")} : ${parametes[1].clr("00FFFF")})`);
        }
    }

    function bossGageInfoProcessor(event) {
        if (!bossId || event.templateId != bossId) {
            bossId = event.templateId;
        
            toolbox.command.message(bossTemplate(bossId));
        }
    }

    function actionStageProcessor(event) {
        if (event.templateId === bossId) {
            toolbox.command.message(actionTemplate(event.skill.id));

            if (data[mapId] && data[mapId][bossId] && data[mapId][bossId][event.skill.id] && data[mapId][bossId][event.skill.id].msg != null) {
                sendMessage(data[mapId][bossId][event.skill.id].msg);
            }
        }
    }

    function dungeonEventMessageProcessor(event) {
        toolbox.command.message(messageTemplate(event.message.replace('@dungeon:', '')));
    }

    function sendMessage(msg) {
        if (!config.notice.enabled) {
            return;
        }

        if (config.notice.party) {
            toolbox.send('C_CHAT', 1, {
                channel: 21,
                message: msg
            });
        } else {
            toolbox.send('S_CHAT', 3, {
                channel: 21,
                authorName: `<font color="#FDD017">BParse<font>`,
                message: msg
            });
        }
	}

    function mapTemplate(mapId) {
        var mapDesc = data[mapId] ? data[mapId].desc : mapId.toString();

        return `${"Map".clr("FDD017")} : ${mapDesc.clr("00FFFF")}`;
    }

    function bossTemplate(bossId) {
        var bossDesc = (data[mapId] && data[mapId][bossId]) ? data[mapId][bossId].desc : bossId.toString();

        return `${"Boss".clr("FDD017")} : ${bossDesc.clr("00FFFF")}`;
    }

    function actionTemplate(actionId) {
        var actionDesc = (data[mapId] && data[mapId][bossId] && data[mapId][bossId][actionId]) ? data[mapId][bossId][actionId].desc : actionId.toString();

        return `${"Action".clr("FDD017")} : ${actionDesc.clr("00FFFF")}`;
    }

    function messageTemplate(message) {
        return `${"Message".clr("FDD017")} : ${message.clr("00FFFF")}`;
    }

    function saveJsonData() {
        fs.writeFileSync(path.join(__dirname, 'data.json'), JSON.stringify(data, null, "\t"));
    }

    function readJsonData() {
        if (!fs.existsSync(path.join(__dirname, 'data.json'))) {
            fs.writeFileSync(path.join(__dirname, 'data.json'), JSON.stringify({}, null, "\t"));
        }

        try {
            return JSON.parse(fs.readFileSync(path.join(__dirname, 'data.json')));
        }
        catch (err) {
            return {};
        }
    }

    this.destructor = () => {
        toolbox.command.remove('bp');
    }
}
